<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome.html" method="POST"> 
        <label for="">First Name:</label><br>
        <input type="text" name="first name"><br><br>
        <label for="">Last Name:</label><br>
        <input type="text" name="last name"><br><br>
        <label for="" name="kelamin">Gender:</label><br><br>
        <input type="radio" value="1" name="gender"> Male <br>
        <input type="radio" value="2" name="gender"> Female <br>
        <input type="radio" value="3" name="gender"> Other <br><br>
        <label for="">Nationality:</label><br><br>
        <select name="nationality" id="">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
            <option value="4">China</option>
        </select><br><br>
        <label for="bahasa" value="bahasa">Language Spoken:</label><br><br>
        <input type="checkbox" value="1" >Indonesia
        <input type="checkbox" value="2" >Melayu
        <input type="checkbox" value="3" >Inggris
        <input type="checkbox" value="4" >Mandarin <br><br>
        <label for="">Bio:</label><br><br>
        <textarea name="bio" id="" cols="35" rows="7"></textarea><br>
        <input type="submit">
    </form>
</body>
</html>